package com.shankar.expandrecycle;

/**
 * Created by Shankar on 06-08-2017.
 */

public class Descript {
    private final String description;
    private final String date;

    public Descript() {
        this.description = "hello";
        this.date = "1/2/2017";
    }

    public String getDescription() {
        return description;
    }

    public String getDate() {
        return date;
    }
}
