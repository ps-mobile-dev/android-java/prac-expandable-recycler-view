package com.shankar.expandrecycle;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bignerdranch.expandablerecyclerview.Adapter.ExpandableRecyclerAdapter;
import com.bignerdranch.expandablerecyclerview.Model.ParentListItem;
import com.bignerdranch.expandablerecyclerview.ViewHolder.ChildViewHolder;
import com.bignerdranch.expandablerecyclerview.ViewHolder.ParentViewHolder;

import java.util.List;

/**
 * Created by Shankar on 06-08-2017.
 */

public class AnnouncementsParentAdapter extends ExpandableRecyclerAdapter<AnnouncementsParentAdapter.MyParentViewHolder, AnnouncementsParentAdapter.MyChildViewHolder> {
    private LayoutInflater mInflater;
    public AnnouncementsParentAdapter(Context context, List<ParentListItem> itemList)
    {
        super(itemList);
        mInflater = LayoutInflater.from(context);
    }
    @Override
    public MyParentViewHolder onCreateParentViewHolder(ViewGroup viewGroup) {
        View view = mInflater.inflate(R.layout.announcement_parent, viewGroup, false);
        return new MyParentViewHolder(view);
    }

    @Override
    public MyChildViewHolder onCreateChildViewHolder(ViewGroup viewGroup) {
        View view = mInflater.inflate(R.layout.announcement_child, viewGroup, false);
        return new MyChildViewHolder(view);
    }

    @Override
    public void onBindParentViewHolder(MyParentViewHolder parentViewHolder, int position, ParentListItem parentListItem) {
        AnnouncementsParent subcategoryParentListItem = (AnnouncementsParent) parentListItem;
        parentViewHolder.lblListHeader.setText(subcategoryParentListItem.getTitle());

    }

    @Override
    public void onBindChildViewHolder(MyChildViewHolder childViewHolder, int position, Object childListItem) {
        Descript subcategoryChildListItem = (Descript) childListItem;
        childViewHolder.txtListChild.setText(subcategoryChildListItem.getDescription());

    }

    public class MyParentViewHolder extends ParentViewHolder {

        public TextView lblListHeader;

        public MyParentViewHolder(View itemView) {
            super(itemView);
            lblListHeader = (TextView) itemView.findViewById(R.id.announcement_title);
        }
    }


    public class MyChildViewHolder extends ChildViewHolder {

        public TextView txtListChild;
        public TextView textView;

        public MyChildViewHolder(View itemView) {
            super(itemView);

            txtListChild = (TextView) itemView.findViewById(R.id.textView);
            textView = (TextView) itemView.findViewById(R.id.textView2);
        }
    }
}

