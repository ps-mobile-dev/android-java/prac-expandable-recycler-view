package com.shankar.expandrecycle;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.bignerdranch.expandablerecyclerview.Model.ParentListItem;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.announcements_view);

        List<AnnouncementsParent> subcategoryParentListItems = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            AnnouncementsParent eachParentItem = new AnnouncementsParent();
            subcategoryParentListItems.add(eachParentItem);
        }


        List<ParentListItem> parentListItems = new ArrayList<>();
        for (AnnouncementsParent subcategoryParentListItem : subcategoryParentListItems) {
            List<Descript> childItemList = new ArrayList<>();
            childItemList.add(new Descript());
            subcategoryParentListItem.setChildItemList(childItemList);
            parentListItems.add(subcategoryParentListItem);
            parentListItems.add(subcategoryParentListItem);
            parentListItems.add(subcategoryParentListItem);
        }
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(new AnnouncementsParentAdapter(MainActivity.this,parentListItems));
    }
}
